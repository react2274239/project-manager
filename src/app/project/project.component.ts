import { Component, Input, inject } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';

import { Project } from '../project';
import { ItemService } from '../item.service';
import { Item } from '../item';
import { ItemComponent } from '../item/item.component';

@Component({
  selector: 'app-project',
  standalone: true,
  imports: [
    MatCardModule,
    ItemComponent,
    CommonModule,
  ],
  templateUrl: './project.component.html',
  styleUrl: './project.component.css'
})
export class ProjectComponent {
  @Input() project!: Project;

  itemService: ItemService = inject(ItemService);

  items: Item[] = [];

  ngOnInit(){
    this
      .itemService
      .getItems(this.project.id)
     .then((items: Item[]) =>{
        this.items = [items[0]];
        console.log(this.items);
      });
  }
}
