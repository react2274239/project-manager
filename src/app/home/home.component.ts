import { Component, inject } from '@angular/core';

import { ProjectService } from '../project.service';
import { Project } from '../project';
import { User } from '../user';
import { ProjectComponent } from '../project/project.component';
import { CommonModule } from '@angular/common';
import { UserComponent } from '../user/user.component';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    CommonModule,
    ProjectComponent,
    UserComponent,
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})

export class HomeComponent {

  projectService: ProjectService = inject(ProjectService);
  userService: UserService = inject(UserService);

  projects: Project[] = [];
  users: User[] = [];

  constructor(){
    this
      .projectService
      .getProjects()
      .then((projects: Project[]) =>{
        this.projects = projects;
        console.log(this.projects);
      });
    this
      .userService
      .getUsers()
      .then((users: User[]) =>{
        this.users = users;
        console.log(this.users);
      });
  }

}
