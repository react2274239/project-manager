export interface Project {
  id: number;
  admin: number;
  name: string;
  description: string;
}
