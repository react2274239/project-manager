import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProjectsComponent } from './projects/projects.component';
import { UsersComponent } from './users/users.component';

export const routes: Routes = [
  {
    title:'Home',
    path:'',
    component: HomeComponent
  },
  {
    title:'Projects',
    path:'projects',
    component: ProjectsComponent
  },
  {
    title:'Users',
    path:'users',
    component: UsersComponent
  }
];
