import { Injectable } from '@angular/core';

import { User } from './user';
import * as Global from './global';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = `${Global.dbUrl}/users`;
  users: User[] = [];

  async getUsers(): Promise<User[]> {
    const data = await fetch(this.url);
    return await data.json() ?? [];
  }

  constructor() { }
}
