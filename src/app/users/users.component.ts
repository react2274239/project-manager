import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

import { User } from '../user';
import { UserComponent } from '../user/user.component';
import { UserService } from '../user.service';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [
    CommonModule,
    UserComponent,
  ],
  templateUrl: './users.component.html',
  styleUrl: './users.component.css'
})
export class UsersComponent {
  userService: UserService = inject(UserService);
  users: User[] = [];

  constructor(){
    this
      .userService
      .getUsers()
      .then((users: User[]) =>{
        this.users = users;
        console.log(this.users);
      });
  }
}
