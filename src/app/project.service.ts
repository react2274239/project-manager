import { Injectable } from '@angular/core';

import { Project } from './project';
import * as Global from './global';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  url = `${Global.dbUrl}/projects`;
  projects: Project[] = [];

  async getProjects(): Promise<Project[]> {
    const data = await fetch(this.url);
    return await data.json() ?? [];
  }

  constructor() { }
}
