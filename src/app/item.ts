export interface Item {
  id: number;
  project_id: number;
  type_id: number;
  state: number;
  priority: number;
  description: string;
}
