import { Injectable } from '@angular/core';

import { Item } from './item';
import * as Global from './global';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  url = `${Global.dbUrl}/items`;

  async getItems(project_id: number): Promise<Item[]> {
    const data = await fetch(this.url + `?project_id=${project_id}`);
    return await data.json() ?? [];
  }
}
