import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectService } from '../project.service';
import { Project } from '../project';
import { ProjectComponent } from '../project/project.component';

@Component({
  selector: 'app-projects',
  standalone: true,
  imports: [
    CommonModule,
    ProjectComponent,
  ],
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.css'
})
export class ProjectsComponent {

  projectService: ProjectService = inject(ProjectService);

  projects: Project[] = [];

  constructor(){
    this
      .projectService
      .getProjects()
      .then((projects: Project[]) =>{
        this.projects = projects;
        console.log(this.projects);
      });
  }

}
